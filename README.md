# orpheus

Sound Sharing Enabled.

## Overview

Orpheus makes use of repository logic to handle the storage, fetch, and query mechanics of git. There's an API that allows files to be stored in a git repo. The outcome is a stored file that has history, versioning, and notes. 

## Running

`make run`

## Building

`make build`

## Container

`registry.gitlab.com/sailr/opensource/orpheus:latest`

## Owner

Sailr.Co Crew
