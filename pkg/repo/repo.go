package repo

import (
	"context"
	"fmt"
	"os"

	"github.com/google/go-github/v28/github"
	"golang.org/x/oauth2"
)

// Client is the github client used by each method
var Client *github.Client

// InitClient creates a new client
func InitClient() {
	ctx := context.Background()
	githubAccessToken := os.Getenv("ORPHEUS_GITHUB_ACCESS_TOKEN")

	accessToken := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: githubAccessToken},
	)

	oauth2Client := oauth2.NewClient(ctx, accessToken)
	Client = github.NewClient(oauth2Client)
}

// GetProjects creates a new repo for storage
func GetProjects() {
	ctx := context.Background()

	opt := &github.RepositoryListByOrgOptions{
		ListOptions: github.ListOptions{PerPage: 10},
	}

	repos, resp, err := Client.Repositories.ListByOrg(ctx, "sailr-ecco", opt)
	fmt.Println(repos)
	fmt.Println(resp)
	fmt.Println(err)
}

// CreateProject creates a new project
func CreateProject(projectName string) {
	ctx := context.Background()
	// create a new private repository named "foo"
	repo := &github.Repository{
		Name:    github.String(projectName),
		Private: github.Bool(false), //make public by default
	}

	repo, _, err := Client.Repositories.Create(ctx, "sailr-ecco", repo)
	if err != nil {
		fmt.Println(err)
		return
	}
}

// DeleteProject removes a project
func DeleteProject() {

}
