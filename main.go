package main

import (
	"gitlab.com/sailr/opensource/orpheus/pkg/server"
	"gitlab.com/sailr/opensource/orpheus/pkg/repo"
)

func main() {
	// init github client
	repo.InitClient()
	// start the server
	server.Start()
}
