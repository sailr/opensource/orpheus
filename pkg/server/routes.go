package server

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/sailr/opensource/orpheus/pkg/repo"
)

// LoadRoutes loads the routes for the router
func LoadRoutes(r *mux.Router){
	loadProjectRoutes(r)
}

// loadProjectRoutes loads the project routes
func loadProjectRoutes(r *mux.Router) *mux.Router{
	// base URI Project path functions
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("alive"))
	})
	r.HandleFunc("/create", func(w http.ResponseWriter, r *http.Request) {
		proj := readBody(r)
		repo.CreateProject(proj.Name)
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte("201 - Created!"))
	})
	r.HandleFunc("/search", func(w http.ResponseWriter, r *http.Request) {
		repo.GetProjects()
	})
	r.HandleFunc("/update", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("200 ok"))
	})
	r.HandleFunc("/delete", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("200 ok"))
	})
	return r
}

// loadFileRoutes
func loadFileRoutes(r *mux.Router) *mux.Router {
	return r
}
