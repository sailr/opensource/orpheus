module gitlab.com/sailr/opensource/orpheus

go 1.13

require (
	github.com/google/go-github/v28 v28.1.1
	github.com/gorilla/mux v1.7.3
	golang.org/x/oauth2 v0.0.0-20180821212333-d2e6202438be
)
