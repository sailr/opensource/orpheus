FROM golang:alpine AS build-stage
WORKDIR /orpheus
RUN apk add --update git
COPY . .
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o orpheus /orpheus

FROM alpine
COPY --from=build-stage /orpheus/orpheus .
EXPOSE 3030
CMD ["./orpheus"]
