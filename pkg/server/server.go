package server

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// Project struct contains the project json
type Project struct {
	Name string `json:"name"`
}

// Start the API server
func Start() {
	r := mux.NewRouter()
	LoadRoutes(r)
	// log and listen
	log.Println("Listening on 3030")
	http.ListenAndServe(":3030", r)
}

func readBody(req *http.Request) *Project {
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		panic(err)
	}
	p := Project{}
	json.Unmarshal([]byte(b), &p)
	return &p
}
